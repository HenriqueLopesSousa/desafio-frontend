import React, { Component } from 'react';
import Comic from './comic';

const Main = (props) => {
  if(props.comicsList.length === 0){
    return <div></div>;
  }
  else {
    return (
      <main>
        {props.comicsList.map(comic => <Comic comic={comic} key={props.comicsList.indexOf(comic)}/>)}
      </main>
    );
  }	
}

export default Main;
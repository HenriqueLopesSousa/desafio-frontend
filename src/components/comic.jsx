import React, { Component } from 'react';

const Comic = (props) => {
  const imageURL = props.comic.thumbnail.path + "." + props.comic.thumbnail.extension;
  return(      
    <div className='comic'>
      <img src={imageURL} alt={imageURL}/>
      <div className='info'>
        <h2>{props.comic.title}</h2>
        <p>{props.comic.dates[0].date.slice(0,10)}</p>
        <p>{props.comic.description ? props.comic.description : "No description"}</p>
      </div>
    </div>
  );
}

export default Comic;

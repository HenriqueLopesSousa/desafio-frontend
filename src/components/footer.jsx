import React, { Component } from 'react';

const Footer = (props) => {
  return (
    <footer>
      <button onClick={props.onLoadComics}>Load</button>
    </footer>
  );
};

export default Footer;
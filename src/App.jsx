import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import Main from './components/main';
import Footer from './components/footer';

class App extends Component {
  state = {
    offset: 0,
    comicsList: []
  }

  handleLoadComics = () => {

    const reqIronManComics = new XMLHttpRequest();
    
    const url = "https://gateway.marvel.com:443/v1/public/characters/1009368/comics?orderBy=-onsaleDate&offset=" 
    + this.state.offset + "&apikey=91d6b13afd2392ea2f2e799b91eb461a";

    reqIronManComics.open("GET",url,true);
    reqIronManComics.send();
    reqIronManComics.onload = () => {
      var jsonIronManComics = JSON.parse(reqIronManComics.responseText);
      this.setState({ 
        comicsList: this.state.comicsList.concat(jsonIronManComics.data.results),
        offset: this.state.offset + 20 
      });
    }
  }

  render() {
    return (
      <React.Fragment>
        <Header />
        <Main comicsList={this.state.comicsList} />
        <Footer onLoadComics={this.handleLoadComics}/>
      </React.Fragment>
    );
  }
}

export default App;

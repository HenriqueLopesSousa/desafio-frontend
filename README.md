Desafio-Frontend Marvel API Iron Man
====================================

API e Lógica
------------

Eu comecei me cadastrando no site https://developer.marvel.com/ para ter acesso a uma chave pública para acessar o API. 
Depois, visitei https://developer.marvel.com/docs para testar requests ao API e entender os resultados.
Assim, aprendi como eles estruturam o JSON e a Request URL.
Então escolhi a "query" que achei que melhor satisfazia a especificação do projeto e copiei a URL.
O programa usa XMLHttpRequest para fazer a consulta ao API com essa URL.
Como o API tem um limite de resultados por vez - 20 por padrão, que pode ser alterado, mas achei 20 uma quantidade boa por vez - eu precisei colocar uma variável "offset" para que cada vez que o programa fizesse uma request ele procurasse a partir de onde parou.
Coloquei o método "handleLoadComics()" no componente "App" junto com as variáveis de estado "offset" e o array "comicsList", que guarda os valores do JSON resultante do XMLHttpRequest (a parte relevante desse JSON está em um array com a chave "results" que por sua vez está dentro do objeto "data").
Toda vez que "handleLoadComics()" é chamada, ela atualiza o array "comicsList" através da concatenação com os novos resultados do API. A concatenação e não a substituição pelos novos valores é necessária para que o aplicativo sempre exiba todas as revistas desde a mais recente, e não apenas 20 de cada vez. 
Ela também atualiza também o "offset" com +20.
O componente "App" então passa o array "comicsList" como uma props para o componente funcional "Main", que verifica se o array está vazio.
Se não estiver, ele usa a função de alta-ordem "map" para criar 20 novos componentes do tipo "Comic", passando para cada um deles a prop "comic" que corresponde a um elemento do array "comicsList". Esse elemento "comic" vai ser usado pelo componente funcional "Comic" para retornar os dados de cada revista.
É o componente "Comic" que extrai a url do thumbnail para a imagem da revista, assim como título, data e descrição, e retorna componentes de html com esses dados.
Já a funão de carregar novas revistas "handleLoadComics()" é acionada por um botão.
A função é passada como props para o componente "Footer" que cria uma tag "footer" com um botão dentro e "levanta" (raises) um evento "onClick" para ser administrado pela função em questão.

Layout
------

O layout simples é feito com CSS3 e flexbox.
O elemento "main" é tratado como um flex e colocado com wrap pra que as revistas apareçam em fileiras umas em cima das outras.
Para que o layout ficasse responsivo, utilizei media queries para que em telas menores a capa da revista ficasse maior e mais visível.


